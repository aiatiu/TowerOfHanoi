import sys
import random
from array import array

############################################################
#********************Game Class*****************************
############################################################
'''
The Board class represents the state of a game (i.e., some combination
of disks on some number of pegs).

To create the starting game state, pass the constructor the number of rods,
number of disks, and target rod that you desire. Rods are zero-indexed, so keep that
in mind when you pass the target rod.

You can see a graphical representation of the game state by calling printGame. The
first rod (rod 0) will be on the top.

You can also construct a gameState using one of the utility functions below.

From a game state, you can move to other game states by calling the makeMove function.
You will need to utilize this function in order to generate the game tree,
so that you can traverse it.

NOTE: You can only create game boards with up to 8 rods.

'''
class Board:

    def __init__(self, numRods, numDisks, targetRod):
        self.rods = []
        self.numDisks = numDisks

        if(numRods < 9):
            self.numRods = numRods
        else:
            raise ValueError("Number of rods must be 8 or less!")

        if(targetRod < numRods):
            self.targetRod = targetRod
        else:
            raise ValueError("Target rod must be < number of rods!")

        for i in range(numRods):
            self.rods.append(array('b'))

        for disk in range(numDisks, 0, -1):
            self.rods[0].append(disk)


    '''
    Returns true once the target rod contains all of the disks

    NOTE: This does NOT validate disk order. It assumes user has
    utilized makeMove function to validate all moves. This validation
    was bypasses for efficiency
    '''
    def isFinished(self):
        return len(self.rods[self.targetRod]) == self.numDisks

    '''
    Moves a disk from a rod to another rod

    Returns the move tuple (disk, fromRod, toRod) if it completes successfully

    If failure, NO MOVE IS MADE
    Returns -1 if the disk cannot be placed on the new rod
    Returns -2 if the old rod is empty
    '''
    def makeMove(self, fromRodIndex, toRodIndex):

        fRod = self.rods[fromRodIndex]

        if len(fRod):
           disk = fRod.pop()
        else:
           return -2

        tRod = self.rods[toRodIndex]
        if not len(tRod) or tRod[len(tRod) - 1] > disk:
            tRod.append(disk)
        else:
            fRod.append(disk)
            return -1

        return (disk, fromRodIndex, toRodIndex)


    '''
    Hashing logic

    Returns a unique integer for every game state

    The hash is a set of 3-bit sequence.
    The numberical value of the ith sequnce represents the rod number
    upon which the ith disk is held. This will be unique for every game
    state.

    Since each combination only has 3 bits, only eight rods
    can be represented in a game if you want gauranteed unique hashes.

    '''
    def hash(self):
        output = 0
        for i, rod in enumerate(self.rods):
            for disk in rod:
                output += i << (3 * (disk - 1))
        return output


    '''
    Instantiates a new game that has all of the same properties
    as the original
    '''

    def makeCopy(self):
        new = Board(self.numRods, self.numDisks, self.targetRod)
        new.rods = []
        for rod in self.rods:
            new.rods.append(array('b', rod))
        return new


    '''
    Utility function that prints out the game horizontally
    '''

    def printBoard(self):
        output = ""
        for rod in self.rods:
            output +="|"
            for disk in rod:
                output += str(disk) + " "
            output += "\n"
        output += "\n"
        sys.stdout.write(output)
        sys.stdout.flush()


    '''
    Returns a list of all of the
    possible children (with their corresponding moves) for this
    game state.

    The list should contain tuples that are formatted as such:
    [(child1, move1), (child2, move2), ... (childn, moven)]

    The makeMove function returns the move structure that you need
    to combine with the children. See the makeMove function for
    more information.
    '''
    def successors(self):
        succ = []

        child = self.makeCopy()

        for fromRod in range(self.numRods):
            for toRod in range(self.numRods):

                if fromRod == toRod:
                    continue

                moveResults = child.makeMove(fromRod, toRod)

                if moveResults == -1:
                    continue
                elif moveResults == -2:
                    break
                else:
                    succ.append((child, moveResults))
                    child = self.makeCopy()
        return succ

    '''
    A heuristic that estimates the amount of moves left
    to complete the game

    This heuristic is consistent
    '''
    def heuristic(self):

        #the estimate of moves left
        val = 0

        # the largest disk not on the target rod
        largestNotOn = -1

        for i in range(self.numDisks, 0, -1):
            if i not in self.rods[self.targetRod]:
                largestNotOn = i
                break

        #finished
        if largestNotOn == -1:
            return 0

        for rod in self.rods:
            if largestNotOn in rod:
                largestNotOnLocation = rod

        val += len(largestNotOnLocation) * 2 - 1

        val += largestNotOn - len(largestNotOnLocation)

        return val



############################################################
#************* Utility Functions ***************************
############################################################

'''
Utility function that returns a random, valid configuration of
disks on the rods
'''
def randomShuffle(numRods, numDisks, targetRod):
    new = Board(numRods, numDisks, targetRod)

    for i in range(len(new.rods)):
        new.rods[i] = array('b')

    for disk in range(numDisks, 0, -1):
        random.choice(new.rods).append(disk)

    return new

'''
Utility function that creates the objective game state
(i.e., all of the disks are placed on the final rod.
'''
def constructTargetBoard(numRods, numDisks, targetRod):
    new = Board(numRods, numDisks, targetRod)

    for i in range(len(new.rods)):
        new.rods[i] = array('b')

    for i in range(numDisks, 0, -1):
        new.rods[targetRod].append(i)

    return new

'''
Utility function that converts a hash value (see hash function above)
into the corresponding game state object.
'''
def constructBoard(hash, numRods, numDisks, targetRod):
    new = Board(numRods, numDisks, targetRod)

    for i in range(len(new.rods)):
        new.rods[i] = array('b')

    for i in range(numDisks, 0, -1):
        bit1 = (hash >> (3 * (i - 1))) & 1
        bit2 = (hash >> (3 * (i - 1) + 1)) & 1
        bit3 = (hash >> (3 * (i - 1) + 2)) & 1

        rodNum = bit1 + (bit2 * 2) + (bit3 * 4)
        new.rods[rodNum].append(i)

    return new

import copy
import queue
import time
from board import *
from priorityq import PQ


############################################################
#*************************** Search ***********************
############################################################
'''
Parent class for the different types of searches. Contains
the mechanic to unwind the path from the parentTrace dictionary
and how to print the path as well.
'''
class Search:
    def __init__(self, start):

        #a mapping of nodes to their parents and the transformation move
        #example: parentTrace[child.hash()] = (parent.hash(), move)
        #this stores games as hashes to avoid object references
        self.parentTrace = {}

        #the starting game state for the search tree
        self.start = start

        #data structures to store the resulting solution paths
        self.gamePath = []
        self.movePath = []
        self.numMoves = 0

        self.end = None

    '''
    Traces the path from the objective state back to the starting
    state using the parentTrace dictionary that is filled during
    the search algorithm
    '''
    def unwindPath(self):

        if self.end:
            target = self.end
        else:
            target = constructTargetBoard(self.start.numRods, self.start.numDisks, self.start.targetRod)

        startHash = self.start.hash()
        nextHash = target.hash()

        self.gamePath = []
        self.movePath = []

        # traces backwards from the target game state
        if not startHash == nextHash:

            while (True):

                self.gamePath.insert(0, constructBoard(nextHash, self.start.numRods, self.start.numDisks, self.start.targetRod))
                if startHash == nextHash:
                    break
                else:
                    moves = self.parentTrace[nextHash][1]
                    self.movePath.insert(0, moves)
                    nextHash = self.parentTrace[nextHash][0]

        self.numMoves = len(self.movePath)

    '''
    Provides a graphical representation of the trace
    '''
    def printPath(self, verbose=False):
        # prints out in order

        if verbose:
            counter = 0
            for i in range(len(self.gamePath)):

                if counter == 0:
                    print("ORIGINAL")
                    print("Heuristic = " + str(self.gamePath[i].heuristic()) + " : Actual Dist = " + str(len(self.gamePath) - counter - 1))
                    self.gamePath[i].printBoard()
                else:
                    print("MOVE " + str(counter) + ": " + str(self.movePath[counter - 1]))
                    print("Heuristic = " + str(self.gamePath[i].heuristic()) + " : Actual Dist = " + str(len(self.gamePath) - counter -1))
                    self.gamePath[i].printBoard()
                print("--------------------------------------")
                counter += 1
        else:
            for i in range(len(self.movePath)):
                print("MOVE " + str(i + 1) + ": " + str(self.movePath[i]))


############################################################
#************* Uninformed DFS Search ***********************
############################################################
'''
 This is an example of how a search of the game tree might look. In this case,
 it is a depth first search. Use this template to familiarize yourself with
 the mechanics of the different data structures.
'''
class DFSSearch(Search):

    def __init__(self, start):
        super().__init__(start)
        self.search()

    '''
    Completes a depth first search in iterative style (vs recursive).

    It stores all of the node relations in the parentTrace dictionary.
    The keys are the hashes of game states.
    The values are tuples with the hash of its parent combined with the move
    necessary to transform the parent into the key (see makeMove description in
    Game class. Upon finding the finished game state, it unwinds the trace
    '''
    def search(self):
        self.parentTrace = {}

        #The search is formatted iteratively (vs. recursively) because python is not optimized for deep recursion
        #and the rescursive solution would either crash the system or take excessively long
        stack = [(self.start, (0,0,0))]

        while(not len(stack) == 0):

            game = stack.pop()[0]
            hash = game.hash()

            #f game is finished, break the loop by returning and ending the search
            #note that there is no need to traverse the entire game tree
            if game.isFinished():

                #before breaking, unwind the path (see parent class)
                self.unwindPath()
                return

            #generate a list of next possible game states
            successors = game.successors()

            #filter out game states that we have already seen
            successors[:] = filter(lambda x: x[0].hash() not in self.parentTrace, successors)

            #record the parent of the next games states and add them to the stack
            for successor in successors:
                self.parentTrace[successor[0].hash()] = (hash, successor[1])
                stack.append(successor)




############################################################
# ************* Uninformed BFS Search ***********************
############################################################
'''
TODO

Using guidance provided by the template above, create a class
that completes a breadth first search (BFS) of the game tree.
'''


class BFSSearch(Search):
    def __init__(self, start, end=None):
        super().__init__(start)

        if end is None:
            self.end = constructTargetBoard(start.numRods,start.numDisks,start.targetRod)
        else:
            self.end = end

        self.endHash = self.end.hash()
        self.parentTrace = {}
        self.search()

    '''
    Completes a breadth first search starting at the indicated starting node to the indicated ending
    node
    '''
    def search(self):
        pass


############################################################
# *************  Uniform Cost Search *************************
############################################################
'''
TODO

Using what you have learned from the DFS and BFS examples, construct a Uniform Cost search
algorithm that creates the desired parent graph.
'''

class UniformCostSearch(Search):

    def __init__(self, start):
        super().__init__(start)

        #the target board
        self.end = constructTargetBoard(start.numRods, start.numDisks, start.targetRod)
        self.endHash = self.end.hash()

        # stores nodes based on g-score
        self.openSet = PQ()
        self.openSet.update(start)

        #stores distances from start for resolved nodes
        self.closedSet = {}

        # stores temporary g-scores
        self.gScore = {start.hash():0}

        # maps nodes to their parents
        self.parentTrace[start.hash()] = {}

        self.search()

    '''
    TODO

    Create your uniform cost search algorithm here.
    '''

    def search(self):
        pass

############################################################
# ************* Informed A* Search *************************
############################################################
'''
TODO

Using what you have learned from the DFS and BFS examples, construct and A* search
algorithm that creates the desired parent graph.
'''

class AStarSearch(Search):
    def __init__(self, start, debug=False):
        super().__init__(start)

        #the target board
        self.end = constructTargetBoard(start.numRods,start.numDisks,start.targetRod)
        self.endHash = self.end.hash()

        #stores nodes based on f-score (g-score + heuristic)
        self.openSet = PQ()
        self.openSet.update(start)

        #stores distances from start for resolved nodes
        self.closedSet = {}

        #stores temporary g-scores
        self.gScore = {start.hash():0}

        #maps nodes to their parents
        self.parentTrace[start.hash()] = {}

        self.search()

    '''
    TODO

    Create your A* search algorithm here.
    '''

    def search(self):
        pass


if __name__ == "__main__":

    board = randomShuffle(5,7,4)

    start = time.clock()
    print("***********************************************DFS SEARCH!*****************************************************")
    DFSSearch(board).printPath(True)
    print("Took " + str(time.clock() - start) + " seconds!\n\n")

    start = time.clock()
    print("***********************************************BFS SEARCH!*****************************************************")
    BFSSearch(board).printPath(True)
    print("Took " + str(time.clock() - start) + " seconds!\n\n")

    start = time.clock()
    print("***********************************************UCS SEARCH!*****************************************************")
    UniformCostSearch(board).printPath(True)
    print("Took " + str(time.clock() - start) + " seconds!\n\n")

    start = time.clock()
    print("***********************************************A* SEARCH!*****************************************************")
    AStarSearch(board).printPath(True)
    print("Took " + str(time.clock() - start) + " seconds!\n\n")
